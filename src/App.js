import logo from './logo.svg';
import './App.css';
import Navbar from './components/Navbar';
import Hero from './components/Hero';
import WhoWeAre from './components/WhoWeAre';
import Card from './components/reusables/Card';
import Footer from './components/Footer';
import { Icon, InlineIcon } from '@iconify/react';
import checkCircle from '@iconify-icons/cil/check-circle';
import plusCircleSolid from '@iconify-icons/clarity/plus-circle-solid';
import minusCircleSolid from '@iconify-icons/clarity/minus-circle-solid';
function App() {
  const cards = [
        {
          title: 'Commercial<br/>construction',
          description: 'We strive to provide complete solutions to any plumbing problems in order to make the process as easy and hassle-free as possible',
          img: './assets/images/commercial-construction.jpg'
        },
        {
          title: 'Maintenance, Residential & Commercial',
          description: 'We strive to provide complete solutions to any plumbing problems in order to make the process as easy and hassle-free as possible',
          img: './assets/images/maintenance.jpg'
        },
        {
          title: 'Emergency & After<br/>hours services',
          description: 'We strive to provide complete solutions to any plumbing problems in order to make the process as easy and hassle-free as possible',
          img: './assets/images/emergency.jpg'
        }
      ];

  return (
    <div>
      <Navbar />
      <Hero />
      <WhoWeAre />
      <section class="what-we-do sec-padding">
        <div className="container">
          <div className="row">
            <div className="col">
              <h2 className="headingfont textDark text-uppercase mb-4 text-center sec-heading mb-md-5">What we do</h2>
            </div>            
          </div>
          <div className="row">          
          {
            cards.map((card) => {
              return (<Card img={ card.img } title={ card.title } description={card.description} />)
            })
          }            
          </div>
          <div className="row pt-5 pb-5 frontlayer">
            <div className="col-md-6 offset-md-6">
              <h2 className="text-white headingfont sec-heading mb-4 text-uppercase">Complete plumbing<br/>solutions</h2>
              <p className="bodyfont text-white">At QLD Coastal Plumbing, our plumbers on the Gold Coast strive to provide complete solutions to any plumbing problems in order to make the process as easy and hassle-free as possible. We offer residential & commercial plumbing services on the Gold coast, Burleigh & Robina.</p>
              <p className="bodyfont text-white">By utilizing a highly efficient, seamless project delivery system and a singular contact point, all Queensland Coastal Plumbing services are designed to get your project completed with minimal interruption to your business operations.</p>
              <a href="#" className="btn btn-primary text-uppercase mt-4">Book a service</a>
            </div>
          </div>
        </div>
        <section className="diagonal">
          <div class="container">
            <div className="row">
              <div className="col-md-3 col-lg-3 text-white text-center text-md-left mb-4 mb-md-0 underline">
                <h3 className="text-white headingfont text-uppercase nocalloutfee">No Call<br/>Out Fee</h3>
              </div>
              <div className="col-md-3 col-lg-3 text-white text-center mb-4 mb-md-0">
                <Icon icon={checkCircle} />
                <p className="checkTitle mt-4 text-uppercase">Local plumbers in Gold Coast</p>
              </div>
              <div className="col-md-3 col-lg-3 text-white text-center mb-4 mb-md-0">
                <Icon icon={checkCircle} />
                <p className="checkTitle mt-4 text-uppercase">Fixed price no upfront cost</p>
              </div>
              <div className="col-md-3 col-lg-3 text-white text-center mb-4 mb-md-0">
                <Icon icon={checkCircle} />
                <p className="checkTitle mt-4 text-uppercase">24 hour service</p>
              </div>
            </div>
          </div>
        </section>
        <div className="container">
          <div className="row pt-5 pb-5 frontlayer">
              <div className="col-md-6">
                <h2 className="text-white headingfont sec-heading mb-4 text-uppercase">Emergency plumbing<br/>solutions</h2>
                <p className="bodyfont text-white">Plumbing emergencies can happen at any time and can range from a block toilet to no hot water to a burst pipe and flooded house.  They can also be very costly and inconvenient.  Our RAPID RESPONSE team are available 24/7 every day of the year to get you out of any plumbing jam.</p>
                <p className="bodyfont text-white">We do not have any call out fees and only charge from the time we arrive on the job.  We understand this can be an overwhelming time.  With a simple after hours call to us, one of our expert technicians will give you advice on immediate action to prevent any further damage to your property before we even walk out the door taking a little bit of pressure off until our commercial plumber of licensed tradespeople arrive.</p>
                <a href="#" className="btn btn-primary text-uppercase mt-4">Book a service</a>
              </div>
            </div>
        </div>
      </section>
      <section className="call-to-action faq sec-padding">
        <div className="container">
          <div className="row">
            <div className="col-md-4 col-lg-6 mb-4 mb-md-0">
              <h2 className="mb-4 text-white sec-heading">Get in touch</h2>
              <div className="contact-item mb-4">
                <p className="text-uppercase font-light font-size-small mb-1 text-white font-family-body">call us</p>
                <p className="mb-0 text-white font-size-large font-family-body font-bold"><a href="tel:1800 367 727">1800 367 727</a></p>
              </div>
              <div className="contact-item mb-4">
                <p className="text-uppercase font-light font-size-small mb-1 text-white font-family-body">Emergency/After Hours Hotline</p>
                <p className="mb-0 text-white font-size-large font-family-body font-bold"><a href="tel:0481 189 032">0481 189 032</a></p>
              </div>
              <div className="contact-item mb-4">
                <p className="text-uppercase font-light font-size-small mb-1 text-white font-family-body">address</p>
                <p className="mb-0 text-white font-size-large font-family-body font-bold">4/14 Rothcote Court Burleigh<br/>Heads QLD 4220</p>
              </div>
            </div>
            <div className="col-md-8 col-lg-6">
              <h2 className="mb-4 fq-dark sec-heading">Frequently Asked</h2>
              <div className="accordion" id="accordionExample">
                <div className="card">
                  <div className="card-header" id="headingOne">
                    <h2 className="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <div className="row align-items-center">
                        <div className="col-10">
                          <p className="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What Areas Do You Service?</p>
                        </div>
                        <div className="col-2 text-center">
                          <div className="accordion-plus">
                            <Icon icon={plusCircleSolid} color="#00c6ff" />
                          </div>
                          <div className="accordion-minus">
                          <Icon icon={minusCircleSolid} color="#00c6ff" />
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div className="body-font textLight card-body font-size-small">
                      QLD Coastal Plumbing provide services to local residents and commercial properties from South Brisbane, throughout the Gold Coasts to Tweed South and surrounding areas.
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="card-header" id="headingTwo">
                    <h2 className="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      <div className="row align-items-center">
                        <div className="col-10">
                        <p className="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What can I expect from my plumber on the Gold Coast?</p>
                        </div>
                        <div className="col-2 text-center">
                          <div className="accordion-plus">
                            <Icon icon={plusCircleSolid} color="#00c6ff" />
                          </div>
                          <div className="accordion-minus">
                          <Icon icon={minusCircleSolid} color="#00c6ff" />
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div className="body-font textLight card-body font-size-small">
                      No call out fees and competitive rates
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="card-header" id="headingThree">
                    <h2 className="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      <div className="row align-items-center">
                        <div className="col-10">
                          <p className="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What are your call out fees?</p>
                        </div>
                        <div className="col-2 text-center">
                          <div className="accordion-plus">
                            <Icon icon={plusCircleSolid} color="#00c6ff" />
                          </div>
                          <div className="accordion-minus">
                          <Icon icon={minusCircleSolid} color="#00c6ff" />
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div className="body-font textLight card-body font-size-small">
                    With our “no call out fee” service guarantee you only pay for time engaged on your job, saving money for local residents all along the Gold Coast.
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="card-header" id="headingFour">
                    <h2 className="mb-0 collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      <div className="row align-items-center">
                        <div className="col-10">
                          <p className="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What services do you offer?</p>
                        </div>
                        <div className="col-2 text-center">
                          <div className="accordion-plus">
                            <Icon icon={plusCircleSolid} color="#00c6ff" />
                          </div>
                          <div className="accordion-minus">
                          <Icon icon={minusCircleSolid} color="#00c6ff" />
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseFour" className="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                    <div className="body-font textLight card-body font-size-small">
                      <p>Qld Coastal Plumbing offer everything plumbing</p>
                      <ul className="pl-0 list-unstyled">
                        <li><b><u>P</u></b>rofessional and progressive advice</li>
                        <li><b><u>L</u></b>egislative compliance</li>
                        <li><b><u>U</u></b>nwavering adherence to quality and service</li>
                        <li><b><u>M</u></b>arket leading rates</li>
                        <li><b><u>B</u></b>ig coverage and serviceability</li>
                        <li><b><u>I</u></b>nnovative solutions saving you time and money</li>
                        <li><b><u>N</u></b>o harm safety policy</li>
                        <li><b><u>G</u></b>old class solutions every time</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </div>
  );
}

export default App;
