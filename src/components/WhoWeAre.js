import React, { Component } from 'react';
class WhoWeAre extends React.Component
{
    render()
    {
        return <section className="who-we-are sec-padding">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-7 mb-4">
                            <img src="./assets/images/who-we-are-thumb.png" className="img-fluid d-block d-lg-none" />
                        </div>
                        <div className="col-lg-5">
                            <h2 className="text-uppercase mb-4 textDark sec-heading text-center text-lg-left">Who are we</h2>
                            <p className="textLight body-font">QLD Coastal Plumbing is a full-service commercial construction and maintenance plumbing company situated in Burleigh Heads on the Gold Coast.</p>
                            <p className="textLight body-font">QCP provide industry leading attention to detail and professionalism, providing all clients with peace of mind that the job will be completed efficiently and in entirety, the first time round.</p>
                        </div>
                    </div>
                </div>
        </section>
    }
} 

export default WhoWeAre;