import React, { Component } from 'react';
import { Icon, InlineIcon } from '@iconify/react';
import phoneIcon from '@iconify-icons/fa-solid/phone';
import hamburgerMenu from '@iconify-icons/radix-icons/hamburger-menu';
import closeBig from '@iconify-icons/vaadin/close-big';
class Navbar extends React.Component
{
    constructor(props) {
        super(props);
        if(window.innerWidth >= 768){
            this.state = {
                isScrolled: false
            }
        }
        else
        {
            this.state = {
                isScrolled: true
            }
        }
        this.state.navhidden = true;
    }

    componentDidMount() {
        if(window.innerWidth >= 768)
            window.addEventListener('scroll', this.listenScrollEvent)
    }

    listenScrollEvent = e => {
        if (window.scrollY > 10) {
          this.setState({isScrolled: true})
        } else {
          this.setState({isScrolled: false})
        }
    }

    toggleNavMenu = () => {
            this.setState({
                navhidden: !this.state.navhidden
            });
        }
    
    render()
    {
        return <header className={'site-header fixed-top ' + (this.state.isScrolled ? 'solid' : 'transparent') }>
                    <nav className="navbar navbar-expand-lg navbar-light">
                        <a className="navbar-brand" href="#">
                            <img src="./assets/images/logo-qcp.png" className="img-fluid" alt="" />
                        </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" onClick={this.toggleNavMenu}>
                            <div className={ (this.state.navhidden ? `d-inline` : `d-none`) }>
                                <Icon icon={hamburgerMenu} />
                            </div>
                            <div className={ (!this.state.navhidden ? `d-inline` : `d-none`) }>
                                <Icon icon={closeBig} />
                            </div>                            
                        </button>
                    
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav ml-auto align-items-center">
                                <li className="nav-item active">
                                    <a className="nav-link text-uppercase" href="#">Home <span className="sr-only">(current)</span></a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link text-uppercase" href="#">About us</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link text-uppercase" href="#">Services</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link text-uppercase" href="#">OH&S</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link text-uppercase" href="#">Contact us</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link text-uppercase" href="#">News</a>
                                </li>
                                <li className="nav-item phone">
                                    <a href="tel:1800 367 727" className="nav-link phone-link d-flex align-items-center">
                                        <span class="phone-icon-span">
                                            <Icon icon={phoneIcon} />
                                        </span>
                                        <span class="ml-3">1800 367 727</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header>
    }
}

export default Navbar;