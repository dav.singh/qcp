import React, { Component } from 'react';
import { Icon, InlineIcon } from '@iconify/react';
import bxlInstagram from '@iconify-icons/bx/bxl-instagram';
import bxlLinkedin from '@iconify-icons/bx/bxl-linkedin';
import bxlFacebook from '@iconify-icons/bx/bxl-facebook';
import phoneIcon from '@iconify-icons/fa-solid/phone';
import envelopeIcon from '@iconify-icons/fa-solid/envelope';
import mapmarkerIcon from '@iconify-icons/fa-solid/map-marker-alt';
class Footer extends React.Component
{
    render()
    {
        return <footer className="site-footer">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-7 order-md-2">
                                <div className="row">
                                    <div className="col-md-5 mb-md-0 mb-4">
                                        <h4 className="font-bold font-family-body text-capitalize font-size-small mb-4 text-white">Quick Links</h4>
                                        <ul className="footerlinks pl-0">
                                            <li><a href="#" className="font-regular font-size-13 text-uppercase text-white">Home</a></li>
                                            <li><a href="#" className="font-regular font-size-13 text-uppercase text-white">About us</a></li>
                                            <li><a href="#" className="font-regular font-size-13 text-uppercase text-white">Services</a></li>
                                            <li><a href="#" className="font-regular font-size-13 text-uppercase text-white">OH&S</a></li>
                                            <li><a href="#" className="font-regular font-size-13 text-uppercase text-white">Contact us</a></li>
                                            <li><a href="#" className="font-regular font-size-13 text-uppercase text-white">News</a></li>
                                        </ul>
                                    </div>
                                    <div className="col-md-7 mb-md-0 mb-4">
                                        <h4 className="font-bold font-family-body text-capitalize font-size-small mb-4 text-white">Get in touch with QCP</h4>
                                        <ul className="footerlinks pl-0 list-unstyled">
                                            <li className="contact-icon-item mb-3">
                                                <div className="double-phone d-flex">
                                                    <div className="double-phone-icon font-size-regular text-white mr-3">
                                                        <Icon icon={phoneIcon} width="18"/>
                                                    </div>
                                                    <div className="double-phone-body">
                                                        <a href="tel:1800 367 727" class="text-white font-family-body font-size-13 font-regular">
                                                            <span>1800 367 727</span>
                                                        </a>
                                                        <br/>
                                                        <a href="tel:0481 189 032" class="text-white font-family-body font-size-13 font-regular">
                                                            <span>0481 189 032 (Emergency/After Hours Hotline)</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li className="contact-icon-item mb-3">
                                                <div className="double-phone d-flex">
                                                    <div className="double-phone-icon font-size-regular text-white mr-3">
                                                        <Icon icon={envelopeIcon} width="18"/>
                                                    </div>
                                                    <div className="double-phone-body">
                                                        <a href="mailto:email@example.com" class="text-white font-family-body font-size-13 font-regular">
                                                            <span>Email Us</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li className="contact-icon-item">
                                                <div className="double-phone d-flex">
                                                    <div className="double-phone-icon font-size-regular text-white mr-3">
                                                        <Icon icon={mapmarkerIcon} width="18"/>
                                                    </div>
                                                    <div className="double-phone-body">
                                                        <span className="text-white font-family-body font-size-13 font-regular">4/14 Rothcote Court Burleigh Heads QLD 4220 PO Box 479 Miami QLD 4220</span>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-5 order-md-1">
                                <img src="./assets/images/footer-logo.png" className="img-fluid ml-auto mr-auto d-table" />
                                <div className="accreditations mt-4">
                                    <p className="text-center mb-0 font-size-tiny body-font font-regular text-uppercase text-white lh-150">Accreditations<br/>QBCC Licence No: 1214559
                                    <br/>QLD Licence No: 26739<br/>NSW Licence No: 227267C</p>
                                </div>
                                <ul className="socialinks d-flex align-items-center justify-content-center list-unstyled mt-4">
                                    <li>
                                        <a href="#">
                                            <Icon icon={bxlFacebook} />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <Icon icon={bxlInstagram} />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <Icon icon={bxlLinkedin} />
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="row copyright mt-4 pt-4">
                            <div className="col">
                                <ul className="mb-0 list-unstyled copyright-links pl-0 font-size-10 d-flex align-items-center text-white font-light text-uppercase justify-content-center text-center">
                                    <li class="mr-3">© 2021 Queensland Coastal Plumbing</li>
                                    <li class="mr-3">
                                        <a href="#">Privacy Policy</a>
                                    </li>
                                    <li>
                                        Designed by <a href="#">Digital Accord</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
    }
}

export default Footer;