import React, { Component } from 'react';
class Card extends React.Component
{
    constructor(props) {
        super();
        this.state = {}
        
    }
    
    render()
    {
        return <div className="col-md-4 mb-4 mb-md-0">
                <div className="cardbox">
                    <div className="cardThumb">
                        <img src={this.props.img} className="w-100 img-fluid"/>
                    </div>
                    <div className="cardBody p-4 bg-white" data-height={this.state.elementHeight}>
                        <h3 class="textDark mb-4 text-center text-capitalize cardHeading" dangerouslySetInnerHTML={{__html: this.props.title}}></h3>
                        <p class="body-font textLight text-center">We strive to provide complete solutions to any plumbing problems in order to make the process as easy and hassle-free as possible</p>
                    </div>
                </div>
            </div>
    }
}

export default Card;