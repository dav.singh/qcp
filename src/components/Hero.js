import React, { Component } from 'react';
import { Icon, InlineIcon } from '@iconify/react';
import chevronRight from '@iconify-icons/carbon/chevron-right';
class Hero extends React.Component
{
    render()
    {
        return <div>
                    <section className="hero position-relative">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-xl-4 col-lg-5">
                                    <h1 className="text-uppercase text-white">Pumbers<br/>Gold Coast</h1>
                                    <p className="text-white mb-4">All commercial & residential<br/>plumbing solutions</p>
                                    <a href="#" class="btn btn-hero no-hover">
                                        <div className="d-flex align-items-center">
                                            <span className="text-uppercase mr-3">Learn more</span>
                                            <div className="iconfontsize">
                                                <Icon icon={chevronRight} />
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="titlebar pt-5 pb-5 d-none d-lg-block">
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="offset-xl-7 col-xl-5 col-lg-6 offset-lg-6">
                                        <p className="mb-0">We offer residential & commercial plumbing services on the Gold coast, Burleigh & Robina.</p>
                                    </div>
                                </div>
                            </div>
                            <img src="./assets/images/who-we-are-thumb.png" className="floating-we-are-thumb" />
                        </div>
                    </section>
                    <div className="titlebar pt-3 pb-4 d-block d-lg-none">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-12 text-center">
                                    <p className="mb-0">We offer residential & commercial plumbing<br/>services on the Gold coast, Burleigh & Robina.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    }
}

export default Hero;